package ru.t1.sarychevv.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.sarychevv.tm.api.service.IConnectionService;
import ru.t1.sarychevv.tm.api.service.IPropertyService;
import ru.t1.sarychevv.tm.api.service.ITaskService;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.enumerated.TaskSort;
import ru.t1.sarychevv.tm.exception.entity.ModelNotFoundException;
import ru.t1.sarychevv.tm.exception.field.*;
import ru.t1.sarychevv.tm.marker.DBCategory;
import ru.t1.sarychevv.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Category(DBCategory.class)
public class TaskServiceTest {

    private static final int NUMBER_OF_ENTRIES = 10;

    private static final String USER_ID_1 = UUID.randomUUID().toString();

    private static final String USER_ID_2 = UUID.randomUUID().toString();

    private static final String TEST_NAME = "Test task";

    private static final String TEST_DESCRIPTION = "Test description";
    @NotNull
    private final IPropertyService propertyService = new PropertyService();
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);
    @NotNull
    private List<Task> taskList;
    @NotNull
    private ITaskService taskService;

    @Before
    public void initRepository() throws Exception {
        taskList = new ArrayList<>();
        taskService = new TaskService(connectionService);
        for (int i = 0; i < NUMBER_OF_ENTRIES; i++) {
            @NotNull final Task task = new Task();
            task.setName(TEST_NAME + i);
            task.setDescription(TEST_DESCRIPTION + i);
            if (i <= 5) task.setUserId(USER_ID_1);
            else task.setUserId(USER_ID_2);
            taskService.add(task);
            taskList.add(task);
        }
    }

    @After
    public void removeRepository() throws Exception {
        taskService.removeAll();
    }

    @Test
    public void testCreate() throws Exception {
        @Nullable final Task task = taskService.create(USER_ID_1, TEST_NAME);
        if (task == null) return;
        Assert.assertEquals(task, taskService.findOneById(task.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, taskService.getSize().intValue());
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithoutName() throws Exception {
        taskService.create(USER_ID_1, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithoutUserId() throws Exception {
        taskService.create(null, TEST_NAME, TEST_DESCRIPTION);
    }

    @Test
    public void testCreateWithDescription() throws Exception {
        @Nullable final Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        Assert.assertEquals(task, taskService.findOneById(task.getId()));
        Assert.assertEquals(NUMBER_OF_ENTRIES + 1, taskService.getSize().intValue());
    }

    @Test(expected = NameEmptyException.class)
    public void testCreateWithDescriptionWithoutName() throws Exception {
        taskService.create(USER_ID_1, null, TEST_DESCRIPTION);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testCreateWithDescriptionWithoutUserId() throws Exception {
        taskService.create(null, TEST_NAME, TEST_DESCRIPTION);
    }

    @Test(expected = DescriptionEmptyException.class)
    public void testCreateWithoutDescription() throws Exception {
        taskService.create(USER_ID_1, TEST_NAME, null);
    }

    @Test
    public void testAdd() throws Exception {
        @NotNull final Task task = new Task();
        taskService.add(task);
        Assert.assertEquals(true, taskService.existsById(task.getId()));
    }

    @Test(expected = ModelEmptyException.class)
    public void testAddWithoutTask() throws Exception {
        @Nullable final Task task = null;
        taskService.add(task);
    }

    @Test
    public void testAddForUser() throws Exception {
        @NotNull final Task task = new Task();
        task.setUserId(USER_ID_1);
        task.setName(TEST_NAME);
        task.setDescription(TEST_DESCRIPTION);
        taskService.add(task);
        Assert.assertEquals(true, taskService.existsById(task.getId()));
    }

    @Test
    public void testExistsById() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        Assert.assertEquals(true, taskService.existsById(task.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void testExistsByIdWithoutId() throws Exception {
        taskService.existsById(null);
    }

    @Test
    public void testExistsByIdForUser() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        Assert.assertEquals(true, taskService.existsById(USER_ID_1, task.getId()));
    }

    @Test(expected = IdEmptyException.class)
    public void testExistsByIdForUserWithoutId() throws Exception {
        taskService.existsById(USER_ID_1, null);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testExistsByIdForUserWithoutUserId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.existsById(null, task.getId());
    }

    @Test
    public void testUpdateById() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое наименование";
        taskService.updateById(USER_ID_1, task.getId(), testName, testDescription);
        Assert.assertEquals(testName, taskService.findOneById(task.getId()).getName());
        Assert.assertEquals(testDescription, taskService.findOneById(task.getId()).getDescription());
    }

    @Test(expected = IdEmptyException.class)
    public void testUpdateByIdWithoutId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        taskService.updateById(USER_ID_1, null, testName, testDescription);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIdWithoutName() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testDescription = "Тестовое описание";
        taskService.updateById(USER_ID_1, task.getId(), null, testDescription);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIdWithoutUserId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        taskService.updateById(null, task.getId(), testName, testDescription);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testUpdateByIdWithWrongId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        taskService.updateById(USER_ID_1, "501", testName, testDescription);
    }

    @Test
    public void testUpdateByIndex() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое наименование";
        taskService.updateByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, testName, testDescription);
        Assert.assertEquals(testName, taskService.findOneById(task.getId()).getName());
        Assert.assertEquals(testDescription, taskService.findOneById(task.getId()).getDescription());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testUpdateByIndexWithoutIndex() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        taskService.updateByIndex(USER_ID_1, null, testName, testDescription);
    }

    @Test(expected = NameEmptyException.class)
    public void testUpdateByIndexWithoutName() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testDescription = "Тестовое описание";
        taskService.updateByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, null, testDescription);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testUpdateByIndexWithoutUserId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        @NotNull String testName = "Тестовое наименование";
        @NotNull String testDescription = "Тестовое описание";
        taskService.updateByIndex(null, NUMBER_OF_ENTRIES / 2 + 1, testName, testDescription);
    }

    @Test
    public void testChangeStatusById() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusById(USER_ID_1, task.getId(), Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskService.findOneById(task.getId()).getStatus());
    }

    @Test(expected = IdEmptyException.class)
    public void testChangeStatusByIdWithoutId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusById(USER_ID_1, null, Status.COMPLETED);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeStatusByIdWithoutUserId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusById(null, task.getId(), Status.COMPLETED);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIdWithoutStatus() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusById(USER_ID_1, task.getId(), null);
    }

    @Test(expected = ModelNotFoundException.class)
    public void testChangeStatusByIdWithWrongId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusById(USER_ID_1, "501", Status.COMPLETED);
    }

    @Test
    public void testChangeStatusByIndex() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, Status.COMPLETED);
        Assert.assertEquals(Status.COMPLETED, taskService.findOneById(task.getId()).getStatus());
    }

    @Test(expected = IndexIncorrectException.class)
    public void testChangeStatusByIdWithoutIndex() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusByIndex(USER_ID_1, null, Status.COMPLETED);
    }

    @Test(expected = UserIdEmptyException.class)
    public void testChangeStatusByIdWithoutUserIndex() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusByIndex(null, NUMBER_OF_ENTRIES / 2 + 1, Status.COMPLETED);
    }

    @Test(expected = StatusEmptyException.class)
    public void testChangeStatusByIndexWithoutStatus() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.changeStatusByIndex(USER_ID_1, NUMBER_OF_ENTRIES / 2 + 1, null);
    }

    @Test
    public void testFindAll() throws Exception {
        Assert.assertEquals(taskService.getSize().intValue(), taskService.findAll().size());
    }

    @Test
    public void testFindAllForUser() throws Exception {
        Assert.assertEquals(taskService.getSize().intValue() / 2 + 1, taskService.findAll(USER_ID_1).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForUserWithoutUserId() throws Exception {
        @Nullable String testUserId = null;
        taskService.findAll(testUserId);
    }

    @Test
    public void testFindAllForUserWithComparator() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        Assert.assertEquals(taskService.getSize().intValue() / 2 + 1, taskService.findAll(USER_ID_1, sort.getComparator()).size());
    }

    @Test(expected = UserIdEmptyException.class)
    public void testFindAllForUserWithComparatorWithoutUserId() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        taskService.findAll(null, sort.getComparator());
    }

    @Test
    public void testFindAllForUserWithComparatorWithoutComparator() throws Exception {
        @NotNull final String sortType = "BY_STATUS";
        @Nullable final TaskSort sort = TaskSort.toSort(sortType);
        Assert.assertEquals(taskService.getSize().intValue() / 2 + 1, taskService.findAll(USER_ID_1, null).size());
    }

    @Test
    public void testFindOneById() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        Assert.assertEquals(TEST_NAME, taskService.findOneById(task.getId()).getName());
        Assert.assertEquals(TEST_DESCRIPTION, taskService.findOneById(task.getId()).getDescription());
    }

    @Test
    public void testFindOneByIdForUser() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        Assert.assertEquals(TEST_NAME, taskService.findOneById(USER_ID_1, task.getId()).getName());
        Assert.assertEquals(TEST_DESCRIPTION, taskService.findOneById(USER_ID_1, task.getId()).getDescription());
    }


    @Test(expected = UserIdEmptyException.class)
    public void testFindOneByIdForUserWithoutUserId() throws Exception {
        @Nullable Task task = taskService.create(USER_ID_1, TEST_NAME, TEST_DESCRIPTION);
        if (task == null) return;
        taskService.findOneById(null, task.getId());
    }

    @Test
    public void testGetSize() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES, taskService.getSize().intValue());
    }

    @Test
    public void testGetSizeForUser() throws Exception {
        Assert.assertEquals(NUMBER_OF_ENTRIES / 2 + 1, taskService.getSize(USER_ID_1));
    }

    @Test(expected = UserIdEmptyException.class)
    public void testGetSizeForUserWithoutUserId() throws Exception {
        taskService.getSize(null);
    }

}
