package ru.t1.sarychevv.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.api.DBConstants;
import ru.t1.sarychevv.tm.api.repository.IUserRepository;
import ru.t1.sarychevv.tm.enumerated.Role;
import ru.t1.sarychevv.tm.model.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    public UserRepository(@NotNull final Connection connection) {
        super(connection);
    }

    @Override
    @NotNull
    protected String getTableName() {
        return DBConstants.TABLE_USER;
    }

    @Override
    public @NotNull User fetch(@NotNull ResultSet row) throws Exception {
        @NotNull final User user = new User();
        user.setId(row.getString(DBConstants.COLUMN_ID));
        user.setLogin(row.getString(DBConstants.COLUMN_LOGIN));
        user.setPasswordHash(row.getString(DBConstants.COLUMN_PASSWORD));
        user.setEmail(row.getString(DBConstants.COLUMN_EMAIL));
        user.setLocked(row.getBoolean(DBConstants.COLUMN_LOCKED));
        user.setFirstName(row.getString(DBConstants.COLUMN_FIRST_NAME));
        user.setLastName(row.getString(DBConstants.COLUMN_LAST_NAME));
        user.setMiddleName(row.getString(DBConstants.COLUMN_MIDDLE_NAME));
        user.setRole(Role.valueOf(row.getString(DBConstants.COLUMN_ROLE)));
        return user;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format("INSERT INTO %s (%s, %s, %s, %s, %s, %s, %s, %s, %s) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)",
                getTableName(), DBConstants.COLUMN_ID, DBConstants.COLUMN_LOGIN,
                DBConstants.COLUMN_PASSWORD, DBConstants.COLUMN_EMAIL,
                DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_FIRST_NAME,
                DBConstants.COLUMN_LAST_NAME, DBConstants.COLUMN_MIDDLE_NAME,
                DBConstants.COLUMN_ROLE);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getId());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPasswordHash());
            statement.setString(4, user.getEmail());
            statement.setBoolean(5, user.getLocked());
            statement.setString(6, user.getFirstName());
            statement.setString(7, user.getLastName());
            statement.setString(8, user.getMiddleName());
            statement.setString(9, user.getRole().toString());
            statement.executeUpdate();
        }
        return user;
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(),
                DBConstants.COLUMN_LOGIN);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, login);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }


    @Nullable
    @Override
    public User findOneByEmail(@NotNull final String email) throws Exception {
        @NotNull final String sql = String.format("SELECT * FROM %s WHERE %s = ? LIMIT 1", getTableName(),
                DBConstants.COLUMN_EMAIL);
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, email);
            @NotNull final ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next()) return null;
            return fetch(resultSet);
        }
    }


    @NotNull
    @Override
    public Boolean isLoginExist(@NotNull final String login) throws Exception {
        return findOneByLogin(login) != null;
    }

    @NotNull
    @Override
    public Boolean isEmailExist(@NotNull final String email) throws Exception {
        return findOneByEmail(email) != null;
    }

    @Override
    public void update(@NotNull final User user) throws Exception {
        @NotNull final String sql = String.format(
                "UPDATE %s SET %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ?, %s = ? WHERE %s = ?",
                getTableName(), DBConstants.COLUMN_LOGIN, DBConstants.COLUMN_PASSWORD, DBConstants.COLUMN_EMAIL,
                DBConstants.COLUMN_LOCKED, DBConstants.COLUMN_FIRST_NAME, DBConstants.COLUMN_LAST_NAME,
                DBConstants.COLUMN_MIDDLE_NAME, DBConstants.COLUMN_ROLE, DBConstants.COLUMN_ID
        );
        try (@NotNull final PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.setString(2, user.getPasswordHash());
            statement.setString(3, user.getEmail());
            statement.setBoolean(4, user.getLocked());
            statement.setString(5, user.getFirstName());
            statement.setString(6, user.getLastName());
            statement.setString(7, user.getMiddleName());
            statement.setString(8, user.getRole().toString());
            statement.setString(9, user.getId());
            statement.executeUpdate();
        }
    }

}
