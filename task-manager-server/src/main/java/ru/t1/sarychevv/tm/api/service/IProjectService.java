package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.Project;
import ru.t1.sarychevv.tm.model.Task;

public interface IProjectService extends IUserOwnedService<Project> {

    @Nullable Project create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable Project create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable Project updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable Project updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable Project changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @Nullable Project changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;

}
