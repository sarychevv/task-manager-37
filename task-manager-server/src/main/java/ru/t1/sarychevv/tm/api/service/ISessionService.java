package ru.t1.sarychevv.tm.api.service;

import ru.t1.sarychevv.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

}
