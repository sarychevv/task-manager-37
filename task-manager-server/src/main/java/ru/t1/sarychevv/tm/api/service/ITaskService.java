package ru.t1.sarychevv.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.enumerated.Status;
import ru.t1.sarychevv.tm.model.Task;

import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @Nullable List<Task> findAllByProjectId(@Nullable String userId, @Nullable String projectId) throws Exception;

    @Nullable Task create(@Nullable String name, @Nullable String description) throws Exception;

    @Nullable Task create(@Nullable String userId, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable Task updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable Task updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

    @Nullable Task changeStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status) throws Exception;

    @Nullable Task changeStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) throws Exception;
}
