package ru.t1.sarychevv.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.sarychevv.tm.dto.request.data.DataBackupLoadRequest;

public class DataBackupSaveCommand extends DataBase64SaveCommand {

    @NotNull
    public static final String DESCRIPTION = "Save backup to file";

    @NotNull
    public static final String NAME = "backup-save";

    @SneakyThrows
    @Override
    public void execute() {
        serviceLocator.getDomainEndpoint().loadDataBackup(new DataBackupLoadRequest(getToken()));
    }

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

}
