package ru.t1.sarychevv.tm.exception.user;

import ru.t1.sarychevv.tm.exception.AbstractException;

public class AuthenticationException extends AbstractException {

    public AuthenticationException() {
        super("Error! Authentication failed...");
    }

}
