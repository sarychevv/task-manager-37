package ru.t1.sarychevv.tm.exception.field;

public final class ModelIdEmptyException extends AbstractFieldException {

    public ModelIdEmptyException() {
        super("Error! Task id is empty...");
    }

}
